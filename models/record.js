const mysql = require('mysql2');
const conn = mysql
    .createConnection({
      
        host: "127.0.0.1",
        user: "root",
        password: "root",
        database: "sensor"
    });


class Record {
    constructor() {
        this.conn = conn;
    }

    recordMapper = (row) => {
        return {
            sensorid: row.sensorId,
            time: row.time,
            temperature: row.temperature
        }
    }

    async getRecords () {
        const [rows, fields] = await this.conn.promise().query("SELECT * FROM record");
        const data = rows.map(this.recordMapper)
        return data;
    }

    async getRecordsId (id) {
        const [rows, fields] = await this.conn.promise().query("SELECT * FROM record WHERE sensorId=? ORDER BY time ASC", [id]);
        // console.log(rows)
        const data = rows.map(this.recordMapper)
        return data;
    }

    async getSensorsStatistics(){
        const [rows, fields] = await this.conn.promise().query("SELECT sensorId, AVG(temperature) as avg, MAX(temperature) as max, MIN(temperature) as min FROM record GROUP BY sensorId ORDER BY sensorId ASC");
        // console.log(rows)
        return rows;
    }


}



module.exports = Record;