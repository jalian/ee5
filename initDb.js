const csv = require("csvtojson");
const mysql = require('mysql2/promise');

// Convert a csv file with csvtojson
csv()
    .fromFile("./MockData.csv")
    .then(async function(jsonArrayObj) { //when parse finished, result will be emitted here.
        console.log(jsonArrayObj);
        const conn = await mysql.createConnection({ host: "127.0.0.1", user: "root", password: "root", database: "sensor" });
        // jsonArrayObj = jsonArrayObj.slice(0, 1000)
        jsonArrayObj.forEach(async e => {
            await conn.execute('INSERT INTO record(sensorId,time,temperature) VALUES(?,?,?)', [e.SensorId, new Date(e.Time).toISOString().slice(0, 19).replace('T', ' '), e.Temperature]);
        })
    })

