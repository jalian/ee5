const express = require('express');
const router = express.Router();
const Record = require('../models/record');


class SencorController {

  constructor() {
    this.router = router;
    this.router.get('/', this.getAll)
    this.router.get('/sensor/:sensorid', this.getSensorById)
    this.router.get('/sensor/record/:sensorid', this.getSensorRecordsById)
  }

  async getAll (req, res) {
    const record = await new Record();
    const response = await record.getSensorsStatistics();
    console.log(response)
    res.render('index', { title: 'Title', data: response, isLoggedin: req.session.isLoggedin });
  }

  async getSensorById (req, res) {
    const id = req.params.sensorid;
    res.render('sensorDetail', { id, isLoggedin: req.session.isLoggedin });
  }
  async getSensorRecordsById (req, res) {
    const record = await new Record();
    const id = req.params.sensorid;
    const response = await record.getRecordsId(id);
    const product = response.map(e =>
      new Date(e.time).toISOString().slice(0, 19).replace('T', ' ')
    );
    const values = response.map(e => e.temperature);
    console.log(values)
    res.send({ product, values });
  }


}


module.exports = new SencorController().router;
