const express = require('express');
const router = express.Router();
const User = require('../models/user')
/* GET users listing. */

class UserController {

  constructor() {
    this.router = router;
    this.router.get('/login', this.login)
    this.router.post('/login', this.postLogin)
    this.router.get('/logout', this.logout)
    this.router.get('/register', this.register)
    this.router.post('/register', this.postRegister)
  }

  login (req, res) {
    if (req.session.isLoggedin) {
      return res.redirect('/')
    }
    const err = req.session.error
    req.session.error = ""
    return res.render('login', { err, isLoggedin: req.session.isLoggedin });
  }

  logout (req, res) {
    req.session.isLoggedin = null;
    return res.redirect('/')
  }

  register (req, res) {
    if (req.session.isLoggedin) {
      return res.redirect('/')
    }
    const err = req.session.error
    req.session.error = ""
    return res.render('register', { err, isLoggedin: req.session.isLoggedin });
  }

  async postRegister (req, res) {
    const user = new User();
    const { username, password, comfirm } = req.body
    if (password !== comfirm) {
      req.session.error = "Password dose not match"
      return res.redirect('/auth/register')
    }
    const arr = await user.getUser(username);
    if (arr.length !== 0) {
      req.session.error = `Username '${username}' is already exist`
      return res.redirect('/auth/register')
    }
    req.session.error = ""
    await user.insertUser(username, password)
    return res.redirect('/auth/login')
  }

  async postLogin (req, res) {
    const user = new User();
    const { username, password } = req.body
    const arr = await user.getUser(username);
    if (arr.length === 0) {
      req.session.error = "Username not found"
      return res.redirect('/auth/login')
    }
    const u = arr[0];
    if (u.password === password) {
      req.session.isLoggedin = true;
      return res.redirect('/')
    } else {
      req.session.error = "Invalid password"
      return res.redirect('/auth/login')
    }

  }
}


module.exports = new UserController().router;
